from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.contrib import admin
from django.utils.safestring import mark_safe
from django import forms

from product.models import (
    Category,
    AttributeValue,
    Attribute,
    Brand,
    ProductImages,
    Product,
    Color,
    Size, Variants
)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'image_tag', 'name', 'slug', 'keywords')
    list_display_links = (
        'id',
        'name',
    )
    search_fields = ('name', 'slug')
    readonly_fields = ['image_tag']
    prepopulated_fields = {'slug': ['name']}

    def image_tag(self, obj):
        if obj.category_photo:
            return mark_safe('<img src="{url}" height="60" />'.format(url=obj.category_photo.url))

    image_tag.short_description = 'Фото'


class AttributeValueInline(admin.TabularInline):
    model = AttributeValue
    extra = 1


class ProductImagesAdminInline(admin.TabularInline):
    """
    Добавляет возможность добавления фото к товару на странице товара,
    а не отдельной записью
    """
    model = ProductImages


class VariantsInline(admin.TabularInline):
    model = Variants
    readonly_fields = (
        'image_tag',
    )
    extra = 1
    show_change_link = True

    def image_tag(self, obj):
        img = ProductImages.objects.get(id=obj.image_id)
        if img.id is not None:
            return mark_safe(f'<img src="{img.image.url}" height="60"/>')
        else:
            return ""


@admin.register(Attribute)
class AttributeAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'category',
        'description',
        'use_in_filter',
        'unit',
    )
    list_display_links = (
        'id',
        'name',
    )
    list_filter = ('category', 'use_in_filter')
    search_fields = ('name',)
    prepopulated_fields = {'slug': ['name']}
    inlines = [AttributeValueInline]


@admin.register(AttributeValue)
class AttributeValueAdmin(admin.ModelAdmin):
    list_display = ('id', 'attribute', 'name', 'description', 'product')
    list_display_links = (
        'id',
        'name',
    )
    list_filter = ('attribute', 'product')
    search_fields = ('name',)


@admin.register(Brand)
class BrandAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'brand_description')
    list_display_links = (
        'id',
        'name',
    )
    search_fields = ('name',)


@admin.register(Color)
class ColorAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'code',
        'color_tag',
    )
    list_display_links = (
        'name',
        'code',
    )

    def color_tag(self, obj):
        if obj.code is not None:
            return mark_safe(f'<p style="background-color:{obj.code}">Color </p>')
        else:
            return ""


@admin.register(ProductImages)
class ProductImagesAdmin(admin.ModelAdmin):
    list_display = ('id', 'image_tag', 'product', 'date_created')
    list_display_links = (
        'id',
        'image_tag',
    )
    readonly_fields = (
        'image_tag',
    )
    list_filter = ('product', 'date_created')

    def image_tag(self, obj):
        if obj.image:
            return mark_safe('<img src="{url}" height="60" />'.format(url=obj.image.url))

    image_tag.short_description = 'Фото'


class ProductAdminForm(forms.ModelForm):
    """
    Форма редактирования товара для администратора
    """
    description = forms.CharField(widget=CKEditorUploadingWidget)

    class Meta:
        model = Product
        fields = '__all__'


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    form = ProductAdminForm

    list_display = (
        'id',
        'image_tag',
        'title',
        'category',
        'slug',
        'price',
        'discount_price',
        'brand',
        'is_active',
        'expired_date',
        'keywords',
    )
    list_display_links = (
        'id',
        'title',
    )
    readonly_fields = ['image_tag']
    list_filter = (
        'category',
        'brand',
        'is_active',
        'date_created',
        'date_updated',
        'expired_date',
    )
    search_fields = ('title', 'category')
    prepopulated_fields = {'slug': ['title']}
    inlines = [
        ProductImagesAdminInline,
        AttributeValueInline,
        VariantsInline,
    ]

    def image_tag(self, obj):
        if not obj.get_main_img() is None:
            return mark_safe('<img src="{url}" height="60" />'.format(url=obj.get_main_img().url))

    image_tag.short_description = 'Фото'


@admin.register(Size)
class SizeAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'code',
    )
    list_display_links = (
        'name',
        'code',
    )


@admin.register(Variants)
class VariantsAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'product',
        'color',
        'size',
        'image_id',
        'image_tag',
        'qty',
        'price',
        'discount_price',
    )
    list_display_links = (
        'title',
        'product',
    )

    def image_tag(self, obj):
        img = ProductImages.objects.get(id=obj.image_id)
        if img.id is not None:
            return mark_safe(f'<img src="{img.image.url}" height="60"/>')
        else:
            return ""


admin.site.site_header = 'Items'
admin.site.site_title = 'Items'
