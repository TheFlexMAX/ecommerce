"""
Данный документ предназначен для хранения всех списков выбора,
который используются в приложении внутри которого он находится
"""

VARIANT_NONE = 'None'
VARIANT_SIZE = 'Size'
VARIANT_COLOR = 'Color'
VARIANT_SIZE_COLOR = 'Size-Color'

VARIANTS = (
    (VARIANT_NONE, 'Отсутствует'),
    (VARIANT_SIZE, 'Размер'),
    (VARIANT_COLOR, 'Цвет'),
    (VARIANT_SIZE_COLOR, 'Размер и цвет'),
)
