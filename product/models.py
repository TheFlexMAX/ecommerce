from django.db import models
from django.urls import reverse
from django.utils import timezone

from product.choices import *
from product.managers import CategoryManager, ProductManager, VariantsManager


class Category(models.Model):
    name = models.CharField(
        max_length=255,
        verbose_name='Category name'
    )
    slug = models.SlugField(
        max_length=255,
        unique=True
    )
    keywords = models.CharField(
        max_length=128,
        null=True,
        blank=True,
        verbose_name='A set of keywords for the internal search engine'
    )
    category_photo = models.ImageField(
        null=True,
        blank=True,
        verbose_name='Picture for category'
    )
    objects = CategoryManager()

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('category_detail', kwargs={'slug': self.slug})


class Attribute(models.Model):
    name = models.CharField(
        max_length=1024,
        verbose_name='Attribute name'
    )
    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE,
        related_name='attributes',
        verbose_name='Category'
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name='Description'
    )
    use_in_filter = models.BooleanField(
        default=False,
        verbose_name='Using the characteristic for the filter'
    )
    unit = models.CharField(
        max_length=50,
        null=True,
        blank=True,
        verbose_name='Unit of measure',
    )
    slug = models.SlugField(
        max_length=255
    )

    class Meta:
        unique_together = ('category', 'name',)
        verbose_name = 'Attribute'
        verbose_name_plural = 'Attributes'

    def __str__(self):
        return f"{self.category.name} | {self.name}"


class AttributeValue(models.Model):
    attribute = models.ForeignKey(
        Attribute,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='related_attributevalue',
        verbose_name='Attribute'
    )
    name = models.CharField(
        max_length=255,
        verbose_name='Attribute value'
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name='Description of value'
    )
    product = models.ForeignKey(
        'Product',
        on_delete=models.CASCADE,
        related_name='related_attributevalue',
        verbose_name='Item'
    )

    class Meta:
        verbose_name = 'Attribute value'
        verbose_name_plural = 'Attribute values'

    def __str__(self):
        return f'{self.attribute} | {self.name}'


class Brand(models.Model):
    name = models.CharField(
        max_length=255,
        verbose_name='Name'
    )
    brand_description = models.TextField(
        verbose_name='Brand Description'
    )

    class Meta:
        verbose_name = 'Brand'
        verbose_name_plural = 'Brands'

    def __str__(self):
        return self.name


class ProductImages(models.Model):
    product = models.ForeignKey(
        'Product',
        on_delete=models.CASCADE,
        verbose_name='Item'
    )
    image = models.ImageField(
        verbose_name='Image'
    )
    date_created = models.DateTimeField(
        default=timezone.now(),
        verbose_name='Date added'
    )

    class Meta:
        verbose_name = 'Product picture'
        verbose_name_plural = 'Product pictures'

    def __str__(self):
        return str(self.image)


class Product(models.Model):
    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE,
        related_name='related_products',
        verbose_name='Category'
    )
    title = models.CharField(
        max_length=255,
        verbose_name='Name'
    )
    slug = models.SlugField(
        max_length=255,
        unique=True
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name='Description'
    )
    price = models.DecimalField(
        max_digits=9,
        decimal_places=2,
        verbose_name='Price'
    )
    discount_price = models.DecimalField(
        max_digits=9,
        decimal_places=2,
        null=True,
        blank=True,
        verbose_name='Discounted price'
    )
    brand = models.ForeignKey(
        Brand,
        on_delete=models.CASCADE,
        related_name='products',
        verbose_name='Brand Name',
        default=None,
        null=True,
        blank=True
    )
    variant = models.CharField(
        max_length=10,
        choices=VARIANTS,
        default=VARIANT_NONE,
        verbose_name='Product variations'
    )
    is_active = models.BooleanField(
        default=True,
        verbose_name='On sale'
    )
    date_created = models.DateTimeField(
        default=timezone.now(),
        verbose_name='Date added'
    )
    date_updated = models.DateTimeField(
        default=timezone.now(),
        verbose_name='Date updated'
    )
    expired_date = models.DateTimeField(
        default=None,
        null=True,
        blank=True,
        verbose_name='Limited supply'
    )
    keywords = models.CharField(
        max_length=128,
        null=True,
        blank=True,
        verbose_name='A set of keywords for the internal search engine'
    )
    objects = ProductManager()

    class Meta:
        verbose_name = 'Item'
        verbose_name_plural = 'Items'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('products:product_detail', kwargs={'category': self.category.slug, 'slug': self.slug})

    def get_main_img(self):
        pi = ProductImages.objects.filter(product=self).order_by('id').first()
        if pi is not None:
            return pi.image
        return pi


class Color(models.Model):
    name = models.CharField(
        max_length=20,
        verbose_name='Name',
    )
    code = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        verbose_name='Code',
    )

    class Meta:
        verbose_name = 'Color'
        verbose_name_plural = 'Colors'

    def __str__(self):
        return self.name


class Size(models.Model):
    name = models.CharField(
        max_length=20,
        verbose_name='Name',
    )
    code = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        verbose_name='Size code',
    )

    class Meta:
        verbose_name = 'Size'
        verbose_name_plural = 'Sizes'

    def __str__(self):
        return self.name


class Variants(models.Model):
    title = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name='Item name',
    )
    product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
        related_name='related_variants',
        verbose_name='Item',
    )
    color = models.ForeignKey(
        Color,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name='related_variants',
        verbose_name='Color',
    )
    size = models.ForeignKey(
        Size,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name='related_variants',
        verbose_name='Size',
    )
    image_id = models.IntegerField(
        blank=True,
        null=True,
        default=0,
        verbose_name='id product pictures',
    )
    qty = models.IntegerField(
        default=1,
        verbose_name='Quantity',
    )
    price = models.DecimalField(
        max_digits=12,
        decimal_places=2,
        default=0,
        verbose_name='Price',
    )
    discount_price = models.DecimalField(
        max_digits=12,
        decimal_places=2,
        null=True,
        blank=True,
        verbose_name='Discounted price'
    )

    objects = VariantsManager()

    class Meta:
        verbose_name = 'Variation'
        verbose_name_plural = 'Variations'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('products:product_variant', kwargs={
            'category': self.product.category.slug,
            'slug': self.product.slug,
            'variant': self.id,
        })

    def image(self):
        img = ProductImages.objects.get(id=self.image_id)
        if img.id is not None:
            varimage = img.image.url
        else:
            varimage = ""
        return varimage
