import json

from django.db.models import Q, Count
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render

from core.utils import get_data_by_page
from product.forms import FilterProductsForm
from product.models import Product, Category, Variants
from product.utils import (
    products_by_filters,
    products_with_discount,
    format_product_cards,
    product_detail_variant_general,
    product_detail_variant, price_category_minmax
)


def product_detail_view(request, category=None, slug=None, color=None, variant=None):
    product_attributes_unique = None
    product = Product.objects.get(category__slug=category, slug=slug)
    variants = Variants.objects.filter(product=product)

    if color or variant:
        pd_data = product_detail_variant(category, product, color, variant)
    else:
        pd_data = product_detail_variant_general(category, product)

    sizes = pd_data.get('sizes', None)
    colors = pd_data.get('colors', None)
    variant_qs = pd_data.get('variant_qs', None)
    product_images = pd_data.get('product_images', None)
    product_main_img = pd_data.get('product_main_img', None)
    product_attributes_category = pd_data.get('product_attributes_category', None)
    product_attributes_values_category = pd_data.get('product_attributes_values_category', None)

    context = {
        'product': product,
        'product_images': product_images[0:],
        'product_attributes_unique': product_attributes_unique,
        'product_attributes_values_category': product_attributes_values_category,
        'sizes': sizes,
        'colors': colors,
        'variant': variant_qs,
    }
    return render(
        request,
        'shop/product_detail.html',
        context
    )


def product_sizes(request):
    post_data = json.loads(request.body.decode("utf-8"))
    color_id = post_data['color_id']
    product_id = post_data['product_id']

    product = Product.objects.none()
    variants = Variants.objects.none()
    if product_id:
        product = Product.objects.filter(id=product_id).first()
    if color_id:
        variants = Variants.objects.filter(product=product, color__id=color_id)
    context = dict(variants=list(
        variants.values('color', 'size__id', 'size__name', 'qty', 'price', 'discount_price',
                        'product_id', 'id').annotate(total=Count('id'))))
    context['color_selected'] = color_id
    return JsonResponse(context)


def product_price(request):
    """
    Отправляет вариант товара с данными, для отображения новой цены
    :param request:
    :return:
    """
    post_data = json.loads(request.body.decode("utf-8"))
    size_id = post_data['size_id']
    product_id = post_data['product_id']
    variant_id = post_data['variant_id']

    product = Product.objects.none()
    variants = Variants.objects.none()

    variant = Variants.objects.filter(id=variant_id, size__id=size_id, product__id=product_id)
    # TODO: сделать возвращение цены для выбранного цвета и выбранного размера

    context = dict(variants=list(
        variant.values('color', 'size__id', 'size__name', 'qty', 'price', 'discount_price').annotate(
            total=Count('id'))))

    return JsonResponse(context)


def categories_view(request):
    """ Отправляет страницу с набором категорий """
    categories = Category.objects.all()
    context = {
        "categories": categories
    }
    return render(
        request,
        'shop/categories.html',
        context
    )


def category_detail_view(request, slug):
    """ Отправляет страницу товаров по категории """

    # формируем форме фильтрации товаров
    filter_form = FilterProductsForm(data=request.GET or None, category_slug=slug)

    category = Category.objects.get(slug=slug)
    category_products = Product.objects.filter(category=category).all()
    product_card_info = format_product_cards(category_products)

    # Запросить минимальную и максимальную цену по категории текущей
    price_data = price_category_minmax(category)
    category_price_min = price_data.get("price_min", 0)
    category_price_max = price_data.get("price_max", 0)

    if filter_form.is_valid():
        category_products = products_by_filters(filter_form, category, category_products)
        product_card_info = format_product_cards(category_products)

        category_products_on_page = get_data_by_page(request, product_card_info)
        context = {
            'category': category,
            'category_products': category_products_on_page,
            'queryset': category_products_on_page,
            'filter_form': filter_form,
            'category_price_min': category_price_min,
            'category_price_max': category_price_max,
        }
        return render(
            request,
            'shop/category_detail.html',
            context
        )

    category_products_on_page = get_data_by_page(request, product_card_info)
    context = {
        'category': category,
        'category_products': category_products_on_page,
        'queryset': category_products_on_page,
        'filter_form': filter_form,
        'category_price_min': category_price_min,
        'category_price_max': category_price_max,
    }
    return render(
        request,
        'shop/category_detail.html',
        context
    )


def search_view(request):
    query = request.GET.get('search', None)
    if query:
        products = Product.objects.filter(
            Q(title__icontains=query) |
            Q(category__name__icontains=query)).order_by('-date_created')
        product_card_info = format_product_cards(products)
        products_on_page = get_data_by_page(request, product_card_info)
        context = {
            'products': products_on_page,
            'queryset': products_on_page,
            'search': query
        }
        return render(request, 'shop/search.html', context)
    else:
        return HttpResponseRedirect('/')


def product_discount_view(request):
    """
    Отправляет страницу товаров со скидками
    """
    category_products = products_with_discount()
    product_card_info = format_product_cards(category_products)

    category_products_on_page = get_data_by_page(request, product_card_info)

    context = {
        'category_products': category_products_on_page,
        'queryset': category_products_on_page,
    }
    return render(request, 'shop/products_discount.html', context)
