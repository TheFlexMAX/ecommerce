from django.urls import path

from product.views import product_detail_view, search_view, product_sizes, product_price

urlpatterns = [
    path('<str:category>/<str:slug>/', product_detail_view, name='product_detail'),
    path('<str:category>/<str:slug>/c<int:color>/', product_detail_view, name='product_variant'),
    path('<str:category>/<str:slug>/v<int:variant>/', product_detail_view, name='product_variant'),
    path('<str:category>/<str:slug>/c<int:color>/v<int:variant>/', product_detail_view, name='product_variant'),
    path('sizes/', product_sizes, name='product_sizes'),
    path('price/', product_price, name='product_price'),
    path('search/', search_view, name='search'),
]
