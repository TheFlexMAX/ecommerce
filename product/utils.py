from django.db.models import Q, Count, QuerySet
from django.forms import Form

from product.choices import VARIANT_SIZE_COLOR, VARIANT_SIZE, VARIANT_COLOR, VARIANT_NONE
from product.models import (
    Attribute,
    AttributeValue,
    Product,
    Variants,
    ProductImages,
    Category,
)


def parse_filter_data(cleaned_data):
    """
    Собирает данные из формы фильтрации
    """
    parsed_data = {
        'brands': cleaned_data.get('brand'),
        'colors': cleaned_data.get('colors'),
        'sizes': cleaned_data.get('sizes'),
        'price_min': cleaned_data.get('price_min'),
        'price_max': cleaned_data.get('price_max'),
        'dynamic': []
    }
    for i, data in enumerate(cleaned_data):
        if i > 2:
            parsed_data['dynamic'].append({
                'key': data[data.find('_') + 1:],
                'value': cleaned_data[data]
            })
    return parsed_data


def products_filter_price(parsed_data: dict, category_products: [Product]) -> [Product]:
    """
    Производит отсев товаров по цене, включая цену в вериантах
    :return:
    """
    if parsed_data['price_min'] is not None and parsed_data['price_max'] is not None:
        price_min = parsed_data['price_min']
        price_max = parsed_data['price_max']

        category_products = category_products.filter(
            # Для обычных товаров
            (Q(price__range=(price_min, price_max), discount_price__isnull=True, related_variants__isnull=True) |
             Q(discount_price__range=(price_min, price_max), price__isnull=False, related_variants__isnull=True)) |
            # Для товаров с вариантами
            (Q(related_variants__price__range=(price_min, price_max), related_variants__discount_price__isnull=True,
               related_variants__isnull=False) |
             Q(related_variants__discount_price__range=(price_min, price_max), related_variants__price__isnull=False,
               related_variants__isnull=False))
        ).distinct()

    return category_products


def products_filter_attrs(category: Category, parsed_data: dict, category_products: [Product]) -> [Product]:
    """
    Производит отсев товаров по дипамическим атрибутам
    :return:
    """
    attributes_category = Attribute.objects.filter(use_in_filter=True, category=category)
    attributes_values_category = AttributeValue.objects.filter(attribute__in=attributes_category)

    filtered_attributes_values = AttributeValue.objects.none()
    for data in parsed_data['dynamic']:
        new_filtered_attributes_values = attributes_values_category.filter(id__in=data.get('value'))
        filtered_attributes_values = filtered_attributes_values | new_filtered_attributes_values

    filtered_products_by_attributes_values = Product.objects.none()
    for attr_value in filtered_attributes_values:
        new_filtered_products = category_products.filter(id=attr_value.product.id)
        filtered_products_by_attributes_values = filtered_products_by_attributes_values | new_filtered_products

    return filtered_products_by_attributes_values


def products_by_filters(filter_form: Form, category: Category, category_products: [Product]) -> [Product]:
    cleaned_data = filter_form.cleaned_data

    parsed_data = parse_filter_data(cleaned_data)

    # Являются ли динамичекие поля (атрибуты) пустыми
    is_empty = True
    for data in parsed_data["dynamic"]:
        data_len = len(data['value'])
        if data_len != 0:
            is_empty = False

    if (not is_empty or (parsed_data['price_min'] is not None and parsed_data['price_max'] is not None) or
            len(parsed_data['brands']) != 0):

        # Отсев по брендам
        if parsed_data['brands'].count() != 0 and parsed_data['brands'] is not None:
            category_products = category_products.filter(brand__in=parsed_data['brands'])

        # Отсев по цветам
        if parsed_data['colors'].count() != 0 and parsed_data['colors'] is not None:
            category_products = category_products.filter(
                related_variants__isnull=False,
                related_variants__color__in=parsed_data['colors']
            )

        # Отсев по размерам
        if parsed_data['sizes'].count() != 0 and parsed_data['sizes'] is not None:
            category_products = category_products.filter(
                related_variants__isnull=False,
                related_variants__size__in=parsed_data['sizes']
            )

        # Собираем новые данные для ответа и отображаем клиенту
        filtered_products_by_attributes_values = products_filter_attrs(category, parsed_data, category_products)

        # Отсев по цене
        if len(filtered_products_by_attributes_values) != 0:
            category_products = products_filter_price(parsed_data, filtered_products_by_attributes_values)
        else:
            category_products = products_filter_price(parsed_data, category_products)
            # category_products = category_products | filtered_products_by_attributes_values
    return category_products


def products_with_discount() -> QuerySet:
    """
    Возвращает все продукты, у которых есть скидки
    :return:
    """
    return Product.objects.filter(
        Q(related_variants__isnull=True, discount_price__isnull=False) |
        Q(related_variants__isnull=False, related_variants__discount_price__isnull=False)
    ).all().distinct()


def format_product_cards(products: [Product]) -> [Product]:
    """
    Форматирует набор карточек со скидками для страницы скидок
    """
    product_card_info = []
    for p in products:
        p_variants = p.related_variants.all()
        min_price_variant = p_variants.order_by('price').first()
        variant_min_price_discount = p_variants.filter(discount_price__isnull=False).order_by('discount_price').first()
        record = {
            'product': p,
            'variants': p_variants,
            'variant_min_price': min_price_variant,
            'variant_min_price_discount': variant_min_price_discount
        }
        product_card_info.append(record)

    return product_card_info


def product_detail_common(product: Product, category_slug: str) -> dict:
    """
    Получает общие данные. Фото и атрибуты товара
    """
    product_main_img = None
    product_attributes_category = None
    product_attributes_values_category = None

    # Получает фото товара
    product_images = ProductImages.objects.filter(product=product)
    if product_images:
        product_main_img = product.get_main_img()

    # Получает атрибуты товара
    category_obj = Category.objects.get(slug=category_slug)
    if category_obj:
        product_attributes_category = Attribute.objects.filter(category=category_obj)
        product_attributes_values_category = AttributeValue.objects.filter(
            attribute__in=product_attributes_category,
            product=product
        )
    return {
        'product_images': product_images,
        'product_main_img': product_main_img,
        'product_attributes_category': product_attributes_category,
        'product_attributes_values_category': product_attributes_values_category,
    }


def product_detail_first_query(product: Product) -> dict:
    """
    Возвращает набор данных для первого запроса описания товара
    """
    colors = None
    sizes = None
    variants = Variants.objects.filter(product=product)

    if product.variant == VARIANT_SIZE_COLOR:
        colors = Variants.objects.colors_by_product_id(product.id)
        sizes = Variants.objects.filter(color__id=colors[0].color.id, product=product)
    if product.variant == VARIANT_SIZE:
        sizes = Variants.objects.filter(product=product)
    if product.variant == VARIANT_COLOR:
        colors = Variants.objects.colors_by_product_id(product.id)

    variant_qs = variants[0]
    return {
        'sizes': sizes,
        'colors': colors,
        'variant_qs': variant_qs
    }


def product_detail_variants(product: Product, color_id: int, variant_id: int) -> dict:
    """
    Возвращает набор данных в соответствии с допустимыми вариантами продукту
    """
    colors = None
    sizes = None
    variant_qs = None
    variants = Variants.objects.filter(product=product)

    if product.variant == VARIANT_SIZE_COLOR:
        colors = Variants.objects.colors_by_product_id(product.id)
        sizes = Variants.objects.filter(color__id=colors[0].color.id, product=product)
        if color_id:
            variant_qs = variants.filter(color__id=color_id).first()
            sizes = Variants.objects.filter(color__id=color_id, product=product)
        elif variant_id:
            variant_qs = variants.filter(id=variant_id).first()

    elif product.variant == VARIANT_COLOR:
        colors = Variants.objects.filter(product=product).values('id', 'color_id').annotate(
            dcount=Count('color_id'))
        # преобразование qs of dict в qs
        temp_colors = Variants.objects.none()
        for c in colors:
            temp_colors = temp_colors | Variants.objects.filter(id=c.get('id', None))
        colors = temp_colors
        if variant_id:
            variant_qs = colors.filter(id=variant_id).first()

    elif product.variant == VARIANT_SIZE:
        sizes = Variants.objects.filter(product=product)
        if variant_id:
            variant_qs = variants.filter(id=variant_id).first()

    return {
        'colors': colors,
        'sizes': sizes,
        'variant_qs': variant_qs
    }


def product_detail_variant(category_slug: str, product: Product, color, variant) -> dict:
    """
    Возвращает набор данных с запрошенным вариантом товара
    """
    sizes = None
    colors = None
    variant_qs = None

    pd_data = product_detail_common(product, category_slug)
    product_images = pd_data.get('product_images', None)
    product_main_img = pd_data.get('product_main_img', None)
    product_attributes_category = pd_data.get('product_attributes_category', None)
    product_attributes_values_category = pd_data.get('product_attributes_values_category', None)

    # Product have variants
    if product.variant != VARIANT_NONE:
        # Первый запрос
        if not color and not variant:
            pd_data = product_detail_first_query(product)
            colors = pd_data.get('colors', None)
            sizes = pd_data.get('sizes', None)
            variant_qs = pd_data.get('variant_qs', None)
        # Выбор цвета или размера
        else:
            pd_data = product_detail_variants(product, color, variant)
            colors = pd_data.get('colors', None)
            sizes = pd_data.get('sizes', None)
            variant_qs = pd_data.get('variant_qs', None)

    return {
        'sizes': sizes,
        'colors': colors,
        'variant_qs': variant_qs,
        'product_images': product_images,
        'product_main_img': product_main_img,
        'product_attributes_category': product_attributes_category,
        'product_attributes_values_category': product_attributes_values_category,
    }


def product_detail_variant_general(category_slug: str, product: Product) -> dict:
    """
    Возвращает первый попавшийся вариант при первом детальном просмотре товара
    """
    sizes = None
    colors = None
    variant_qs = None

    pd_data = product_detail_common(product, category_slug)
    product_images = pd_data.get('product_images', None)
    product_main_img = pd_data.get('product_main_img', None)
    product_attributes_category = pd_data.get('product_attributes_category', None)
    product_attributes_values_category = pd_data.get('product_attributes_values_category', None)

    if product.variant != VARIANT_NONE:
        pd_details = product_detail_first_query(product)
        sizes = pd_details.get('sizes', None)
        colors = pd_details.get('colors', None)
        variant_qs = pd_details.get('variant_qs', None)

    return {
        'sizes': sizes,
        'colors': colors,
        'variant_qs': variant_qs,
        'product_images': product_images,
        'product_main_img': product_main_img,
        'product_attributes_category': product_attributes_category,
        'product_attributes_values_category': product_attributes_values_category,
    }


def price_product_variant(products: [Product], variants: [Variants], is_min: bool) -> float:
    """
    Ищет максимальную и минимальную цену для товаров учитывая вариативность
    """
    # TODO: Refactor me
    price_common = None
    price_discount = None
    price = None

    if is_min:
        price_product = (products.filter(price__gte=0).order_by('price').first()).price
        price_product_discount = (
            products.filter(discount_price__gte=0).order_by('discount_price').first())
        if price_product_discount is None:
            price_product_discount = 0
        else:
            price_product_discount = price_product_discount.discount_price
        price_variant = (variants.filter(price__gte=0).order_by('price').first())
        price_variant_discount = (variants.filter(discount_price__gte=0).order_by('discount_price').first())
    else:
        price_product = (products.filter(price__gte=0).order_by('-price').first()).price
        price_product_discount = (
            products.filter(discount_price__gte=0).order_by('-discount_price').first())
        if price_product_discount is None:
            price_product_discount = 0
        else:
            price_product_discount = price_product_discount.discount_price
        price_variant = (variants.filter(price__gte=0).order_by('-price').first())
        price_variant_discount = (variants.filter(discount_price__gte=0).order_by('-discount_price').first())

    if is_min:
        if price_variant:
            price_variant = price_variant.price
            price_common = price_product
            if price_variant < price_product:
                price_common = price_variant
            if price_variant_discount:
                price_variant_discount = price_variant_discount.discount_price
                price_discount = price_product_discount
                if price_variant_discount < price_product_discount:
                    price_discount = price_variant_discount
        else:
            price_common = price_product
            price_discount = price_product_discount
    else:
        if price_variant:
            price_variant = price_variant.price
            price_common = price_product
            if price_variant > price_product:
                price_common = price_variant
            if price_variant_discount:
                price_variant_discount = price_variant_discount.discount_price
                price_discount = price_product_discount
                if price_variant_discount > price_product_discount:
                    price_discount = price_variant_discount
        else:
            price_common = price_product
            price_discount = price_product_discount

    # Минимальное из двух оставшихся
    if is_min:
        price = price_common
        if price_discount and price_common:
            if price_common < price_discount:
                price = price_common
            elif price_common > price_discount:
                price = price_discount
    # Максимальное
    else:
        price = price_common
        if price_discount and price_common:
            if price_common < price_discount:
                price = price_discount
            elif price_common > price_discount:
                price = price_common

    return price


def price_category_minmax(category: Category):
    """
    Возвоащает самую большую и самую минимальную цену в переданной категории
    """
    price_min = 0
    price_max = 0
    products_category = Product.objects.filter(category=category)
    products_variants = Variants.objects.filter(product__in=products_category)
    price_min = price_product_variant(products_category, products_variants, is_min=True)
    price_max = price_product_variant(products_category, products_variants, is_min=False)
    context = {
        'price_min': price_min,
        'price_max': price_max,
    }
    return context
