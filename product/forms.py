from decimal import Decimal

from django import forms
from django.core.validators import MinValueValidator
from django.forms import ModelMultipleChoiceField

from product.models import Brand, Product, Attribute, Variants, Color, Size
from product.utils import price_product_variant


class FilterModelMultipleChoiceField(ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return obj.name


class FilterProductsForm(forms.Form):
    """
    Форма для фильтрации товаров в определенной категории
    """
    brand = forms.ModelMultipleChoiceField(
        queryset=Brand.objects.none(),
        widget=forms.CheckboxSelectMultiple,
        required=False,
    )
    brand.label = 'Бренд'
    price_min = forms.DecimalField(
        max_digits=20,
        min_value=0,
        decimal_places=2,
        required=False,
        validators=[MinValueValidator(Decimal('1.00'))],
    )
    price_min.label = 'Цена минимальная'
    price_max = forms.DecimalField(
        max_digits=20,
        min_value=0,
        decimal_places=2,
        required=False,
        validators=[MinValueValidator(Decimal('1.00'))],
    )
    price_max.label = 'Цена максимальная'
    colors = forms.ModelMultipleChoiceField(
        queryset=Color.objects.none(),
        widget=forms.CheckboxSelectMultiple,
        required=False,
    )
    colors.label = 'Цвет'
    sizes = forms.ModelMultipleChoiceField(
        queryset=Size.objects.none(),
        widget=forms.CheckboxSelectMultiple,
        required=False,
    )
    sizes.label = 'Размер'

    def __init__(self, category_slug=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        products_category = Product.objects.filter(category__slug=category_slug)
        products_variants = Variants.objects.filter(product__in=products_category)
        brands_of_product = products_category.values('brand')
        self.fields['brand'].queryset = Brand.objects.filter(id__in=brands_of_product)
        self.fields['colors'].queryset = Color.objects.filter(
            related_variants__isnull=False,
            related_variants__product__in=products_category
        ).distinct()
        self.fields['sizes'].queryset = Size.objects.filter(
            related_variants__isnull=False,
            related_variants__product__in=products_category
        ).distinct()

        self.fields['price_min'].initial = price_product_variant(products_category, products_variants, is_min=True)
        self.fields['price_max'].initial = price_product_variant(products_category, products_variants, is_min=False)

        attributes_category = Attribute.objects.filter(use_in_filter=True, category__slug=category_slug)
        for i, attribute in enumerate(attributes_category):
            self.fields[f'parameter_{attribute.slug}'] = FilterModelMultipleChoiceField(
                queryset=attribute.related_attributevalue.all(),
                widget=forms.CheckboxSelectMultiple,
                required=False
            )
            self.fields[f'parameter_{attribute.slug}'].label = attribute.name
