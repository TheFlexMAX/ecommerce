# Cart
from django.urls import path

from order.views import cart_view, add_to_cart_view, delete_from_cart_view, change_qty_cart_view, checkout_view, \
    make_order_view

urlpatterns = [
    path('cart/', cart_view, name='cart'),
    path('add-to-cart/<str:category>/<str:slug>/', add_to_cart_view, name='add_to_cart'),
    path('add-to-cart/<str:category>/<str:slug>/v<int:variant>/', add_to_cart_view, name='add_to_cart'),
    path('remove-from-cart/<str:category>/<str:slug>/', delete_from_cart_view, name='delete_from_cart'),
    path('remove-from-cart/<str:category>/<str:slug>/v<int:variant>', delete_from_cart_view, name='delete_from_cart'),
    path('change-qty-cart/<str:category>/<str:slug>/', change_qty_cart_view, name='change_qty_cart'),
    path('change-qty-cart/<str:category>/<str:slug>/v<int:variant>', change_qty_cart_view, name='change_qty_cart'),
    # Orders
    path('checkout/', checkout_view, name='checkout'),
    path('make-order/', make_order_view, name='make_order'),
]
