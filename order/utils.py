from django.conf import settings
from django.core.mail import get_connection, EmailMultiAlternatives
from django.db import models
from django.template.loader import render_to_string

from core.utils import get_active_staff_emails
from order.forms import ShippingAddressForm
from order.models import Cart, CartProduct, ShippingAddress
from product.models import Category, Product, Variants
from user.models import Customer


def get_active_customer_cart(request):
    """ Получает корзину для текущего пользователя """
    if request.user.is_authenticated:
        customer = Customer.objects.filter(user=request.user).first()
        cart = Cart.objects.filter(owner=customer, in_order=False).first()
        if not customer:
            customer = Customer.objects.create(
                user=request.user
            )
        if not cart:
            cart = Cart.objects.create(owner=customer)
    else:
        session_key = request.session.session_key
        cart = Cart.objects.filter(for_anonymous_user=True, session_key=session_key, in_order=False).first()
        if not cart:
            cart = Cart.objects.create(for_anonymous_user=True, session_key=session_key)
    return cart


def recalculate_cart(cart):
    """ Делает перерасчет суммарной стоимости заказа, а также кол-ва товара """
    cart_data = cart.products.aggregate(models.Sum('final_price'), models.Sum('qty'))
    if cart_data.get('final_price__sum'):
        cart.final_price = cart_data['final_price__sum']
    else:
        cart.final_price = 0
    if cart_data.get('qty__sum'):
        cart.total_products = cart_data['qty__sum']
    else:
        cart.total_products = 0
    cart.save()


def controller_product_cart(request, category, slug, action, variant=None):
    """ Осуществляет управление продуктами в корзине """
    category_obj = Category.objects.get(slug=category)
    product_obj = Product.objects.get(slug=slug)
    cart = get_active_customer_cart(request)
    if request.user.is_anonymous:
        if variant:
            variant_obj = Variants.objects.filter(id=variant).first()
            cart_product, is_created = CartProduct.objects.get_or_create(
                session_key=request.session.session_key,
                cart=cart,
                product=product_obj,
                variant=variant_obj,
            )
        else:
            cart_product, is_created = CartProduct.objects.get_or_create(
                session_key=request.session.session_key,
                cart=cart,
                product=product_obj
            )
    else:
        if variant:
            variant_obj = Variants.objects.filter(id=variant).first()
            cart_product, is_created = CartProduct.objects.get_or_create(
                user=cart.owner,
                cart=cart,
                product=product_obj,
                variant=variant_obj,
            )
        else:
            cart_product, is_created = CartProduct.objects.get_or_create(
                user=cart.owner,
                cart=cart,
                product=product_obj
            )

    if action == 'add':
        if is_created:
            cart.products.add(cart_product)
        elif not is_created and cart_product.variant:
            cart.products.add(cart_product)
        cart_product.save()
        recalculate_cart(cart)
    elif action == 'remove':
        cart.products.remove(cart_product)
        cart_product.delete()
        recalculate_cart(cart)
    elif action == 'change_qty':
        qty = int(request.POST.get('qty'))
        cart_product.qty = qty
        cart_product.save()
        recalculate_cart(cart)


def create_new_shipping_address(request):
    """
    Создает новый адресс для заказа
    """
    if request.user.is_anonymous:
        customer = None
    else:
        customer = Customer.objects.get(user=request.user)

    # shipping_address_form_set = modelformset_factory(
    #     ShippingAddress,
    #     fields=('address', 'city', 'region', 'zipcode'),
    #     extra=1
    # )
    # formset = shipping_address_form_set(request.POST or None)
    # clean_data_formset = formset.cleaned_data[0]
    form = ShippingAddressForm(request.POST or None)
    if form.is_valid():
        cleaned_data = form.cleaned_data
        shipping_address = ShippingAddress.objects.filter(
            region=cleaned_data['region'],
            city=cleaned_data['city'],
            address=cleaned_data['address'],
            customer=customer
        ).first()
        if not shipping_address:
            shipping_address = ShippingAddress.objects.create(
                region=cleaned_data['region'],
                city=cleaned_data['city'],
                address=cleaned_data['address'],
                customer=customer
            )
            shipping_address.save()
        return shipping_address
    return None


def send_order_message(order):
    """
    Отправляет письма администраторам о получении нового заказа
    """

    email = None
    if order.customer:
        email = order.customer.user.email

    order_data = {
        'id': order.id,
        'date': order.date_created,
        'first_name': order.first_name,
        'last_name': order.last_name,
        'email': email,
        'phone': order.phone,
        'region': order.shipping_address.region,
        'city': order.shipping_address.city,
        'address': order.shipping_address.address,
        'zipcode': order.shipping_address.zipcode,
        'products': order.cart.products.all(),
        'final_price': order.cart.final_price,
        'order': order,
    }
    subject = f'Новый заказ #{order.id}'
    message = render_to_string('email/email_new_order_admin.html', order_data)

    connection = get_connection(
        username=settings.EMAIL_HOST_USER,
        password=settings.EMAIL_HOST_PASSWORD,
        fail_silently=False
    )
    emails = []
    for staff_email in get_active_staff_emails():
        email = EmailMultiAlternatives(
            subject=subject,
            body=message,
            from_email=settings.EMAIL_HOST_USER,
            to=staff_email
        )
        email.attach_alternative(message, "text/html")
        emails.append(email)
    connection.send_messages(emails)
