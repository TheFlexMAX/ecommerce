from django import forms
from django.contrib import admin
from django.db.models import Count, Sum, Min, Max, DateTimeField
from django.db.models.functions import Trunc
from django.utils.safestring import mark_safe

from order.models import OrderSummary, CartProduct, Cart, ShippingAddress, Order


def get_next_in_date_hierarchy(request, date_hierarchy):
    if date_hierarchy + '__day' in request.GET:
        return 'hour'
    if date_hierarchy + '__month' in request.GET:
        return 'day'
    if date_hierarchy + '__year' in request.GET:
        return 'week'
    return 'month'


class CartProductInline(admin.TabularInline):
    model = CartProduct
    fields = (
        'image_tag',
        'product',
        'variant',
        'qty',
        'final_price',
    )
    readonly_fields = (
        'image_tag',
    )
    extra = 1

    def image_tag(self, obj):
        img = obj.product.get_main_img()
        if img is not None:
            return mark_safe(f'<img src="{img.url}" height="60"/>')
        else:
            return ""


class CartInline(admin.TabularInline):
    model = Cart
    extra = 1


@admin.register(OrderSummary)
class OrderSummaryAdmin(admin.ModelAdmin):
    """
    Отображает графики о суммарных продажах
    """
    change_list_template = 'admin/order_summary_change_list.html'
    date_hierarchy = 'date_created'

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(
            request,
            extra_context=extra_context,
        )
        try:
            qs = response.context_data['cl'].queryset
        except (AttributeError, KeyError):
            return response
        metrics = {
            'total': Count('cart__products__product__title'),
            'total_sales': Sum('cart__final_price'),
        }
        response.context_data['summary'] = list(
            qs
                .values('cart__products__product__title')
                .annotate(**metrics)
                .order_by('-total_sales')
        )
        response.context_data['summary_total'] = dict(
            qs.aggregate(**metrics)
        )

        period = get_next_in_date_hierarchy(request, self.date_hierarchy)
        response.context_data['period'] = period

        summary_over_time = qs.annotate(
            period=Trunc('date_created', period, output_field=DateTimeField()),
        ).values('period').annotate(total=Sum('cart__final_price')).order_by('period')

        summary_range = summary_over_time.aggregate(
            low=Min('total'),
            high=Max('total'),
        )
        high = summary_range.get('high', 0)
        low = summary_range.get('low', 0)
        response.context_data['summary_over_time'] = [{
            'period': x['period'],
            'total': x['total'] or 0,
            'pct': ((x['total'] or 0) - low) / (high - low) * 100
            if high > low else 0,
        } for x in summary_over_time]

        return response


@admin.register(CartProduct)
class CartProductAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'image_tag',
        'user',
        'cart',
        'product',
        'qty',
        'final_price',
        'session_key',
    )
    list_display_links = (
        'id',
        'cart',
        'user',
    )
    readonly_fields = ['image_tag']
    list_filter = ('user', 'cart', 'product', 'final_price')

    def image_tag(self, obj):
        if obj.product.get_main_img():
            return mark_safe('<img src="{url}" width="40" height="60" />'.format(url=obj.product.get_main_img().url))

    image_tag.short_description = 'Фото'


@admin.register(Cart)
class CartAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'owner',
        'total_products',
        'final_price',
        'date_created',
        'in_order',
        'for_anonymous_user',
        'session_key',
    )
    list_display_links = (
        'id',
        'owner',
    )
    inlines = [
        CartProductInline,
    ]
    list_filter = ('owner', 'date_created', 'in_order', 'for_anonymous_user')
    raw_id_fields = ('products',)


@admin.register(ShippingAddress)
class ShippingAddressAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'customer',
        'region',
        'city',
        'address',
        'zipcode',
        'date_added',
    )
    list_display_links = (
        'id',
        'customer',
    )
    list_filter = ('customer', 'date_added')


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'customer',
        'first_name',
        'last_name',
        'phone',
        'cart',
        'shipping_address',
        'status',
        'comment',
        'date_created',
        'date_order',
    )
    list_display_links = (
        'id',
        'customer',
    )
    list_filter = (
        'customer',
        'cart',
        'shipping_address',
        'date_created',
        'date_order',
    )


admin.site.site_header = 'Orders'
admin.site.site_title = 'Orders'
