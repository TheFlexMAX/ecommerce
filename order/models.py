from django.contrib.sites.models import Site
from django.db import models
from django.urls import reverse
from django.utils import timezone

from order.choices import STATUS_CHOICES, STATUS_NEW
from product.models import Product, Variants
from user.models import Customer


class Cart(models.Model):
    owner = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        verbose_name='Owner',
    )
    products = models.ManyToManyField(
        'CartProduct',
        blank=True,
        related_name='related_cart',
        verbose_name='Basket items'
    )
    total_products = models.PositiveIntegerField(
        default=0,
        verbose_name='Total items',
    )
    final_price = models.DecimalField(
        default=0,
        max_digits=9,
        decimal_places=2,
        verbose_name='Total price'
    )
    date_created = models.DateTimeField(
        default=timezone.now(),
        verbose_name='Creation date'
    )
    in_order = models.BooleanField(
        default=False,
        verbose_name='On order'
    )
    for_anonymous_user = models.BooleanField(
        default=False,
        verbose_name='For an anonymous user'
    )
    session_key = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name='Session key'
    )

    class Meta:
        verbose_name = 'Cart'
        verbose_name_plural = 'Carts'

    def __str__(self):
        return f'Cart {str(self.owner)}'


class CartProduct(models.Model):
    user = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        verbose_name='Buyer',
    )
    cart = models.ForeignKey(
        Cart,
        on_delete=models.CASCADE,
        related_name='related_products',
        verbose_name='Cart',
    )
    product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
        verbose_name='Item',
    )
    variant = models.ForeignKey(
        Variants,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name='Product variant',
    )
    qty = models.PositiveIntegerField(
        default=1,
        verbose_name='Quantity',
    )
    final_price = models.DecimalField(
        max_digits=9,
        decimal_places=2,
        verbose_name='Total price'
    )
    session_key = models.CharField(
        max_length=255,
        default=None,
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = 'Shopping cart item'
        verbose_name_plural = 'Shopping cart items'

    def __str__(self):
        return f'Item: {self.product.title} (cart)'

    def save(self, *args, **kwargs):
        if self.variant is not None:
            if self.variant.discount_price is not None:
                self.final_price = self.qty * self.variant.discount_price
            else:
                self.final_price = self.qty * self.variant.price
        else:
            if self.product.discount_price is not None:
                self.final_price = self.qty * self.product.discount_price
            else:
                self.final_price = self.qty * self.product.price
        super().save(*args, **kwargs)


class ShippingAddress(models.Model):
    customer = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        verbose_name='Buyer'
    )
    region = models.CharField(
        max_length=255,
        verbose_name='Region'
    )
    city = models.CharField(
        max_length=255,
        verbose_name='City'
    )
    address = models.CharField(
        max_length=255,
        verbose_name='Address'
    )
    zipcode = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name='Zip Code'
    )
    date_added = models.DateTimeField(
        default=timezone.now(),
        verbose_name='Date added'
    )

    class Meta:
        verbose_name = 'Shipping address'
        verbose_name_plural = 'Shipping addresses'

    def __str__(self):
        return f'{self.city} {self.address}'


class Order(models.Model):
    customer = models.ForeignKey(
        Customer,
        related_name='related_orders',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        verbose_name='Buyer',
    )
    first_name = models.CharField(
        max_length=255,
        verbose_name='First name'
    )
    last_name = models.CharField(
        max_length=255,
        verbose_name='Last name'
    )
    phone = models.CharField(
        max_length=20,
        verbose_name='Phone number'
    )
    cart = models.ForeignKey(
        Cart,
        on_delete=models.CASCADE,
        related_name='related_cards',
        verbose_name='Cart',
    )
    shipping_address = models.ForeignKey(
        ShippingAddress,
        on_delete=models.CASCADE,
        verbose_name='Shipping address'
    )
    status = models.CharField(
        max_length=128,
        verbose_name='Order status',
        choices=STATUS_CHOICES,
        default=STATUS_NEW
    )
    comment = models.TextField(
        null=True,
        blank=True,
        verbose_name='Order comment',
    )
    date_created = models.DateTimeField(
        default=timezone.now(),
        verbose_name='Order Date'
    )
    date_order = models.DateField(
        default=timezone.now,
        null=True,
        blank=True,
        verbose_name='Date of receipt of order',
    )

    class Meta:
        verbose_name = 'Order'
        verbose_name_plural = 'Orders'

    def __str__(self):
        return f'{self.id} - Order number'

    def get_admin_url(self):
        domain = Site.objects.get_current().domain
        path = reverse('admin:%s_%s_change' % (self._meta.app_label, self._meta.model_name),
                       args=[self.id])
        return 'http://%s%s' % (domain, path)


class OrderSummary(Order):
    class Meta:
        proxy = True
        verbose_name = 'Sale total'
        verbose_name_plural = 'Sales total'
