STATUS_NEW = 'new'
STATUS_IN_PROGRESS = 'in_progress'
STATUS_READY = 'ready'
STATUS_COMPLETE = 'complete'

STATUS_CHOICES = (
    (STATUS_NEW, 'новый'),
    (STATUS_IN_PROGRESS, 'в процессе'),
    (STATUS_READY, 'готов'),
    (STATUS_COMPLETE, 'завершен'),
)
