from django import forms

from order.models import Order, ShippingAddress


class OrderForm(forms.ModelForm):

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     self.fields['date_order'].label = 'Дата получения заказа'
    #
    # date_order = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'}))
    first_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Имя'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Фамилия'}))
    phone = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Номер тел.'}))
    comment = forms.CharField(required=False, widget=forms.TextInput(attrs={'placeholder': 'Комментарий'}))

    class Meta:
        model = Order
        fields = (
            'first_name', 'last_name', 'phone', 'comment'
        )


class ShippingAddressForm(forms.ModelForm):

    class Meta:
        model = ShippingAddress
        fields = (
            'region', 'city', 'address', 'zipcode'
        )

    def __init__(self, *args, **kwargs):
        super(ShippingAddressForm, self).__init__(*args, **kwargs)
        self.fields['address'].widget.attrs['placeholder'] = "Улица"
        self.fields['city'].widget.attrs['placeholder'] = "Город"
        self.fields['region'].widget.attrs['placeholder'] = "Область"
        self.fields['zipcode'].widget.attrs['placeholder'] = "Почтовый индекс"
