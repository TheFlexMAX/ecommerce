from datetime import timedelta

from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils import timezone
from django.views.decorators.http import require_http_methods

from core.utils import create_new_order
from order.forms import ShippingAddressForm, OrderForm
from order.utils import get_active_customer_cart, controller_product_cart, create_new_shipping_address, \
    send_order_message
from product.models import Category
from user.models import Customer


def cart_view(request):
    """ Отправляет страницу корзины товаров """
    cart = get_active_customer_cart(request)
    cart_products = cart.products.all()
    context = {
        'cart_products': cart_products,
        'cart': cart
    }
    return render(request, 'shop/cart.html', context)


def add_to_cart_view(request, category, slug, variant=None):
    """ Добавляет товар в корзину пользователя """
    controller_product_cart(request, category, slug, 'add', variant)
    # messages.add_message(request, messages.INFO, 'Товар добавлен в корзину')
    return HttpResponseRedirect('/order/cart/')


def delete_from_cart_view(request, category, slug, variant=None):
    """ Удаляет товар из корзины пользователя """
    controller_product_cart(request, category, slug, 'remove', variant)
    # messages.add_message(request, messages.INFO, 'Товар удален из корзины')
    return HttpResponseRedirect('/order/cart/')


@require_http_methods(["POST"])
def change_qty_cart_view(request, category, slug, variant=None):
    """ Метод уменьшения количества заказанных товаров """
    controller_product_cart(request, category, slug, 'change_qty', variant)
    # messages.add_message(request, messages.INFO, 'Количество изменено')
    return HttpResponseRedirect('/order/cart/')


def checkout_view(request):
    """ Страница оформления заказа """
    customer = request.user
    cart = get_active_customer_cart(request)
    categories = Category.objects.get_categories_for_navbar()
    cart_products = cart.products.all()

    delivery_date = timezone.now() + timedelta(days=14)
    formset = ShippingAddressForm(request.POST or None)
    form = OrderForm(request.POST or None)
    context = {
        'cart': cart,
        'categories': categories,
        'cart_products': cart_products,
        'delivery_date': delivery_date,
        'form': form,
        'formset': formset
    }
    return render(request, 'shop/checkout.html', context)


@transaction.atomic
def make_order_view(request):
    """ Осуществляет подтверждение заказа от пользователя """
    cart = get_active_customer_cart(request)
    form = OrderForm(request.POST or None)
    if request.user.is_anonymous:
        customer = None
    else:
        customer = Customer.objects.get(user=request.user)

    if form.is_valid():
        shipping_address = create_new_shipping_address(request)
        new_order = create_new_order(customer, form, cart, shipping_address)
        if customer is not None:
            customer.orders.add(new_order)
        # Отправляем письмо администраторам
        send_order_message(order=new_order)

        return HttpResponseRedirect('/')

    context = {
        'cart': cart,
        'form': form
    }
    return HttpResponseRedirect('/order/checkout/')
