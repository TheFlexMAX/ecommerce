let data = {};


document.addEventListener('DOMContentLoaded', () => {
    /*  Подкреплять события запросов на кнопки выбороа цвета */

    let colorValues = [...document.getElementsByClassName('color-value')];

    colorValues.forEach(colorValue => {
       colorValue.addEventListener('click', queryVariants);
    });
})

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        let cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            let cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


async function queryVariants() {
    /* Запрашивает доступные размеры на сервере */
    let url = '/product/sizes/';
    let product_id = this.dataset.product_id;
    let color_id = this.dataset.color_id;
    let csrf = getCookie('csrftoken');

    let response = await fetch(url, {
        method: 'POST',
        headers: {
            "Accept": "application/json",
            'Content-Type': 'application/json',
            "X-CSRFToken": csrf,
            "X-Requested-With": "XMLHttpRequest",
        },
        body: JSON.stringify({
            product_id,
            color_id,
        })
    });

    if (response.ok) { // если HTTP-статус в диапазоне 200-299
        // получаем тело ответа (см. про этот метод ниже)
        data = await response.json();
        renderVariants();
        updateChangeSizeListener();
    } else {
        alert("Ошибка HTTP: " + response.status);
    }
}

function renderVariants() {
    /* Перерисовка значений размеров выпадающего списка */
    renderSizes();
    setPriceFirst();
}

function setPriceFirst() {
    let priceContainer = document.getElementsByClassName('product-about__price-container')[0];
    let variant = variantBySizeID(data.variants[0].size__id);
    priceContainer.innerHTML = '';
    if (variant.discount_price) {
        let newH2 = document.createElement('h2');
        let newDel = document.createElement('del');
        newDel.textContent = variant.price;
        newH2.appendChild(newDel);

        let newH4 = document.createElement('h4');
        newH4.textContent = variant.discount_price;
        newH4.classList.add('text--red');

        priceContainer.appendChild(newH2);
        priceContainer.appendChild(newH4);
    }
    else {
        let newH4 = document.createElement('h4');
        newH4.textContent = variant.price;
        priceContainer.appendChild(newH4);
    }
}

function renderSizes() {
    let productSizes = document.getElementById('product_size');

    productSizes.innerHTML = '';
    console.log(data);
    data.variants.forEach(variant => {
        let size_id = variant.size__id;
        let size_name = variant.size__name;
        let newOpt = document.createElement('option');
        newOpt.value = size_id;
        newOpt.textContent = size_name;
        newOpt.setAttribute('data-variant_id', variant.id);
        newOpt.setAttribute('data-product_id', variant.product_id);
        productSizes.appendChild(newOpt);
    });
}

function variantBySizeID(id) {
    let variant_ret = null;
    data.variants.forEach(variant => {
        if (variant.size__id == id) {
            variant_ret = variant;
        }
    });
    if (variant_ret) return variant_ret;
    return {};
}

function updateChangeSizeListener() {
    let productSizes = document.getElementById('product_size');
    productSizes.addEventListener('change', updatePrice);
}

async function updatePrice() {
    let priceContainer = document.getElementsByClassName('product-about__price-container')[0];
    let sizeSelect= document.getElementById('product_size');
    let size_id = sizeSelect.selectedOptions[0].value;
    let product_id = sizeSelect.selectedOptions[0].dataset.product_id;
    let variant_id = sizeSelect.selectedOptions[0].dataset.variant_id;
    // Запросить данные о цене
    let url = '/product/price/';
    let csrf = getCookie('csrftoken');

    let response = await fetch(url, {
        method: 'POST',
        headers: {
            "Accept": "application/json",
            'Content-Type': 'application/json',
            "X-CSRFToken": csrf,
            "X-Requested-With": "XMLHttpRequest",
        },
        body: JSON.stringify({
            product_id,
            size_id,
            variant_id,
        })
    });

    if (response.ok) {
        let variant_price = await response.json();
        let variant = variant_price.variants[0];
        priceContainer.innerHTML = '';
        if (variant.discount_price) {
            let newH2 = document.createElement('h2');
            let newDel = document.createElement('del');
            newDel.textContent = variant.price;
            newH2.appendChild(newDel);

            let newH4 = document.createElement('h4');
            newH4.textContent = variant.discount_price;
            newH4.classList.add('text--red');

            priceContainer.appendChild(newH2);
            priceContainer.appendChild(newH4);
        }
        else {
            let newH4 = document.createElement('h4');
            newH4.textContent = variant.price;
            priceContainer.appendChild(newH4);
        }

        updateChangeSizeListener();
    } else {
        alert("Ошибка HTTP: " + response.status);
    }
}
