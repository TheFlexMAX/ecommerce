document.addEventListener( 'DOMContentLoaded', function () {

    let primarySlider = new Splide( '#primary-slider', {
        type        : 'fade',
        heightRatio : 0.4,
        pagination  : false,
        arrows      : true,
        arrowPath   : 'm',
        cover       : true,
        rewind      : true,
        autoplay    : true,
        pauseOnHover: true,
        fade        : true,
        height      : '500px',
        classes: {
            arrows: 'splide__arrows viewer__arrows',
            arrow : 'splide__arrow viewer__arrow',
            prev  : 'splide__arrow--prev main__arrow--prev',
            next  : 'splide__arrow--next main__arrow--next',
        },
    } ).mount(); // do not call mount() here.
} );
