
document.addEventListener( 'DOMContentLoaded', function () {

    let loginDropdown = document.getElementsByClassName('profile-dropdown-container')[0];
    let profileHeaderBtn = document.getElementsByClassName('header__button--notClickable')[0];

    profileHeaderBtn.addEventListener('click', function (event) {
        if (loginDropdown.classList.contains('hidden')) {
            loginDropdown.classList.remove('hidden');
        }
        else {
            loginDropdown.classList.add('hidden');
        }
    });

    // TODO: Доделать на метод при наведении мыши
    // let currentElem = null;
    //
    // loginDropdown.addEventListener('mouseover', function (event) {
    //
    //     let target = event.target.closest('div');
    //
    //     // переход не на <td> - игнорировать
    //     if (!target) return;
    //
    //     // переход на <td>, но вне нашей таблицы (возможно при вложенных таблицах)
    //     // игнорировать
    //     if (!loginDropdown.contains(target)) return;
    //
    //     // ура, мы зашли на новый <td>
    //     currentElem = target;
    //     target.style.background = 'pink';
    //
    //     if (loginDropdown.classList.contains('hidden')) {
    //         loginDropdown.classList.remove('hidden');
    //     }
    // });
    //
    // loginDropdown.addEventListener('mouseover', function (event){
    //     // если мы вне <td>, то игнорируем уход мыши
    //     // это какой-то переход внутри таблицы, но вне <td>,
    //     // например с <tr> на другой <tr>
    //     if (!currentElem) return;
    //
    //     // мы покидаем элемент – но куда? Возможно, на потомка?
    //     let relatedTarget = event.relatedTarget;
    //
    //     while (relatedTarget) {
    //         // поднимаемся по дереву элементов и проверяем – внутри ли мы currentElem или нет
    //         // если да, то это переход внутри элемента – игнорируем
    //         if (relatedTarget == currentElem) return;
    //
    //         relatedTarget = relatedTarget.parentNode;
    //     }
    //
    //     // мы действительно покинули элемент
    //     currentElem.style.background = '';
    //     currentElem = null;
    //     if (!loginDropdown.classList.contains('hidden')) {
    //         loginDropdown.classList.add('hidden');
    //     }
    // });

});
