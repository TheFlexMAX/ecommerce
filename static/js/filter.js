function checkList () {
    let span = this.parentNode;
    if (span.classList.contains('visible')) {
        span.classList.remove('visible');
    } else {
        span.classList.add('visible');
    }
}

let spanOfCheckLists = document.querySelectorAll("[class^='parameter']");

spanOfCheckLists.forEach(span => {
    span.addEventListener('click', checkList);
});

let brandSpan = document.getElementsByClassName("brand")[0];
brandSpan.addEventListener('click', () => {
    brandSpan.addEventListener('click', checkList);
});

let colorSpan = document.getElementsByClassName("colors")[0];
colorSpan.addEventListener('click', () => {
    colorSpan.addEventListener('click', checkList);
});

let sizeSpan = document.getElementsByClassName("sizes")[0];
sizeSpan.addEventListener('click', () => {
    sizeSpan.addEventListener('click', checkList);
});

let inputPriceMin = document.getElementById('id_price_min');
let inputPriceMax = document.getElementById('id_price_max');

let slider = document.getElementsByClassName('price-slider-horizontal')[0];

let sliderPrice = noUiSlider.create(slider, {
    start  : [priceMin, priceMax],
    connect: true,
    range: {
        'min': priceMinCategory,
        'max': priceMaxCategory,
    }
});

slider.noUiSlider.on('update', function (values, handle) {

    let value = values[handle];

    if (handle) {
        inputPriceMax.value = value;
    } else {
        inputPriceMin.value = value;
    }
});

inputPriceMin.addEventListener('change', function () {
    slider.noUiSlider.set([this.value, null]);
});

inputPriceMax.addEventListener('change', function () {
    slider.noUiSlider.set([null, this.value]);
});
