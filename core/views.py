from django.shortcuts import render

from .models import (
    Post,
    SliderPost,
    Faq
)
from .utils import (
    get_data_by_page,
    get_products_for_main_page,
    get_most_popular,
)


def base_view(request):
    """ Отправляет базовое представление (главную страницу) пользователю """
    session_key = request.session._session_key
    trend_products = get_products_for_main_page()
    popular_products = get_most_popular(5)
    slides = SliderPost.objects.filter(is_active=True).order_by('-show_order')
    context = {
        'trend_products': trend_products,
        'popular_products': popular_products,
        'slides': slides
    }
    return render(request, 'shop/main_page.html', context)


def about_view(request):
    """
    Присылает страницу о нас
    """
    context = {}
    return render(request, 'shop/about.html', {})


def blog_view(request):
    """
    Запрашивает блог
    """
    posts = Post.objects.all().order_by('-date_created')
    posts_on_page = get_data_by_page(request, posts)
    context = {
        'posts': posts_on_page,
        'queryset': posts_on_page,
    }
    return render(request, 'shop/blog.html', context)


def blog_post_detail_view(request, id):
    """
    Запрашивает пост блога
    """
    post = Post.objects.filter(id=id).first()
    context = {
        'post': post
    }
    return render(request, 'shop/blog_post.html', context)


def delivery_view(request):
    """
    Возвращает информацию о способах доставки
    """
    context = {}
    return render(request, 'shop/delivery.html', context)


def faq_view(request):
    """
    Возвращает страницу с часто задаваемыми вопросами
    """
    faq = Faq.objects.all()
    context = {
        'faq': faq,
    }
    return render(request, 'shop/faq.html', context)
