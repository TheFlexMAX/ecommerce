from django.urls import path

from product.views import categories_view, product_discount_view, category_detail_view
from .views import (
    base_view,
    about_view,
    blog_view,
    blog_post_detail_view,
    faq_view,
    delivery_view,
)

urlpatterns = [
    # Navigation
    path('', base_view, name='base'),
    path('about/', about_view, name='about'),
    # blog
    path('blog/', blog_view, name='blog'),
    path('blog/post/<int:id>', blog_post_detail_view, name='blog_detail'),
    # category
    path('categories/', categories_view, name='categories'),
    path('category/discount/', product_discount_view, name='product_discount'),
    path('category/<str:slug>/', category_detail_view, name='category_detail'),
    # faq
    path('faq/', faq_view, name='faq_view'),
    # delivery
    path('delivery/', delivery_view, name='delivery_view'),
]
