from django import forms

from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.db.models import Count, Sum, DateTimeField, Min, Max, DateField
from django.db.models.functions import Trunc
from django.utils.safestring import mark_safe

from .models import *


from django.contrib import admin

from .models import (
    Product,
    Specifications,
    Faq,
    Post
)


@admin.register(Faq)
class FaqAdmin(admin.ModelAdmin):
    list_display = ('id', 'question', 'answer')
    list_display_links = (
        'id',
        'question',
    )


class PostAdminForm(forms.ModelForm):
    """
    Форма редактирования поста блога для администратора
    """
    content = forms.CharField(widget=CKEditorUploadingWidget)

    class Meta:
        model = Post
        fields = '__all__'


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    form = PostAdminForm

    list_display = (
        'id',
        'title',
        'date_created',
        'content',
        'preview_content',
        'preview_img',
    )
    list_display_links = (
        'id',
        'title',
    )
    list_filter = ('date_created',)


@admin.register(MainPost)
class MainPostAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'product',
        'content',
        'banner_img',
        'show_order',
        'date_created',
        'is_active',
    )
    list_display_links = (
        'id',
        'product',
    )
    list_filter = ('product', 'date_created', 'is_active')


@admin.register(SliderPost)
class SliderPostAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'title',
        'image_tag',
        'show_order',
        'is_active',
    )
    list_display_links = (
        'id',
        'title',
        'image_tag',
    )

    def image_tag(self, obj):
        if obj.banner_img:
            return mark_safe('<img src="{url}" height=60 />'.format(url=obj.banner_img.url))


admin.site.site_header = 'Online store'
admin.site.site_title = 'Online store'
