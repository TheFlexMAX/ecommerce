from django.core.paginator import Paginator, EmptyPage
from django.db.models import Sum
from django.contrib.auth.models import User

from order.models import CartProduct
from .models import Product, MainPost


def get_data_by_page(request, data):
    """
    Возвращает набор фиксированных данные по странице для переданного набора данных
    """
    paginator = Paginator(data, 16)
    page_num = request.GET.get('page', None)
    if page_num is None:
        page_num = request.GET.get('page', 1)

    try:
        data_on_page = paginator.page(page_num)
    except EmptyPage:
        data_on_page = paginator.page(1)
    return data_on_page


def get_products_for_main_page():
    return MainPost.objects.filter(is_active=True).order_by('show_order')


def get_most_popular(qty):
    """
    Получает набор самых покупаемых товаров
    """
    cart_products = list(CartProduct.objects.values('product__id').all().annotate(Sum('qty')).order_by('qty__sum'))[:5]
    ids = [id['product__id'] for id in cart_products]
    cart_products = Product.objects.filter(id__in=ids)
    return cart_products


def create_new_order(customer, form, cart, shipping_address):
    """
    Создает новый заказ
    """
    # Создаем новый заказ
    cleaned_data = form.cleaned_data

    new_order = form.save(commit=False)
    new_order.first_name = cleaned_data['first_name']
    new_order.last_name = cleaned_data['last_name']
    new_order.phone = cleaned_data['phone']
    new_order.cart = cart
    new_order.shipping_address = shipping_address
    new_order.comment = cleaned_data['comment']
    if cart.for_anonymous_user:
        new_order.customer = None
    else:
        new_order.customer = customer
    new_order.save()

    cart.in_order = True
    cart.save()

    return new_order


def get_active_staff_emails():
    """
    Получает указанную электронную почту для персонала
    """
    return User.objects.filter(is_staff=True, is_active=True).values_list('email')

