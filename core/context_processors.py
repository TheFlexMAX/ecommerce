from order.utils import get_active_customer_cart
from product.models import Category


def get_cart_total_products(request):
    return {
        'total_products': get_active_customer_cart(request).total_products
    }


def get_categories(request):
    return {
        'categories': Category.objects.get_categories_for_navbar()
    }
