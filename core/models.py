from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse
from django.utils import timezone

from product.models import Product


class Specifications(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    name = models.CharField(max_length=255, verbose_name='The name of the product for the characteristic')

    class Meta:
        verbose_name = 'Specification'
        verbose_name_plural = 'Specifications'

    def __str__(self):
        return f'Characteristics for the product {self.name}'


class Faq(models.Model):
    question = models.CharField(
        max_length=255,
        verbose_name='Question'
    )
    answer = models.CharField(
        max_length=2048,
        verbose_name='Answer'
    )

    class Meta:
        verbose_name = 'Question - answers'
        verbose_name_plural = 'Question - answers'

    def __str__(self):
        return self.question


class Post(models.Model):
    title = models.CharField(
        max_length=255,
        verbose_name='Title'
    )
    date_created = models.DateTimeField(
        default=timezone.now(),
        verbose_name='Creation date'
    )
    content = models.TextField(
        max_length=10000,
        verbose_name='Contents of the record'
    )
    preview_content = models.TextField(
        default=None,
        null=True,
        blank=True,
        verbose_name='Summary'
    )
    preview_img = models.ImageField(
        null=True,
        default=True,
        verbose_name='Preview image',
    )

    class Meta:
        verbose_name = 'Blog post'
        verbose_name_plural = 'Blog posts'

    def __str__(self):
        return f'{self.id} | {self.title}'


class SliderPost(models.Model):
    title = models.CharField(
        max_length=128,
        verbose_name='Slide name'
    )
    banner_img = models.ImageField(
        null=False,
        blank=False,
        verbose_name='Image'
    )
    show_order = models.PositiveIntegerField(
        null=False,
        blank=False,
        default=1,
        verbose_name='Display priority'
    )
    date_created = models.DateTimeField(
        default=timezone.now(),
        verbose_name='Creation date'
    )
    is_active = models.BooleanField(
        default=False,
        verbose_name='Display on the page'
    )

    class Meta:
        verbose_name = 'Slide'
        verbose_name_plural = 'Slides'

    def __str__(self):
        return f'{self.show_order} | {self.title}'


class MainPost(models.Model):
    product = models.ForeignKey(
        Product,
        null=False,
        blank=False,
        on_delete=models.DO_NOTHING,
        verbose_name='Described product'
    )
    content = models.TextField(
        max_length=1000,
        verbose_name='Contents of the record'
    )
    banner_img = models.ImageField(
        null=False,
        blank=False,
        verbose_name='Image'
    )
    show_order = models.PositiveIntegerField(
        null=False,
        blank=False,
        default=1,
        verbose_name='The order in which the record is displayed among the others'
    )
    date_created = models.DateTimeField(
        default=timezone.now(),
        verbose_name='Creation date'
    )
    is_active = models.BooleanField(
        default=False,
        verbose_name='Display on the page'
    )

    class Meta:
        verbose_name = 'Main entry'
        verbose_name_plural = 'Entries on the home page'

    def __str__(self):
        return f'{self.show_order} | {self.content.title}'

    def get_absolute_url(self):
        return reverse('blog_detail', kwargs={'id': self.id})
