
# Ecommerce

## Development requires
- Python 3.8.9

## Installation
First one you should create virtual environment in project <br/>

### Create virtual environment
First one you should create virtual environment. For this open your cmd and 
use this command:
``` Bash
python -m venv venv
```
After previous steps activate your virtual environment.
``` Bash
venv\Scripts\activate.bat
```

### Install requirements
You should install requirements. For this you need "freeze" module. 
For installing requirements use this command in terminal:
```Bash
python -m pip install -r requirements.txt
```

### Install ckeditor
- Step 1 <br/>
In shop/settings.py find this lines and comment it <br/>
```Python
# STATICFILES_DIRS = [
#     os.path.join(BASE_DIR, 'static'),
# ]
```

- Step 2 <br/>
  Uncomment this line
```Python
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
```

- Step 3 <br/>
End change DEBUG=True To False
```Python
DEBUG=False
```

- Step 4 <br/>
After that make this command in terminal
```Bash
python manage.py collectstatic
```

When terminal ask you that "Are you sure want to do this?" Enter command:
```Bash
yes
```

After this make all steps in reverse beginning from step 1 to step 3

### Create DB
When ckeditor was installed, now you should create DB. <br/>
For that make this commands:
```Bash
python manage.py makemigrations
python manage.py migrate
```

### Create superuser
After all steps you should create admin for web resource using this command:
```Bash
python manage.py createsuperuser
```

### Load basic data
Insert to command line this commands  
```Bash
python manage.py loaddata fixtures\color.json
python manage.py loaddata fixtures\size.json
```

### Connect google account for email sending 
For emailing in project using SMTP technology. The following will describe the process of using SMTP for google

#### Step 1
Create common google accounts with email address which you prefer for using in e-commerce

### Step 2
Visit this [link](https://mail.google.com/mail/u/0/#settings/fwdandpop)
and switch on IMAP access for you commerce account

#### Step 3
Visit [this](https://myaccount.google.com/u/1/lesssecureapps?pli=1&rapt=AEjHL4Og5Fingq6cnCabgSnDDzZmMl89a7A-qknbYnLnwEPuxI4-qnNiCn-l3N0zpJ5uVNUxMpdGvmywkXeZBwA3RDKTwH2-5w)
link and switch on access from Insecure Apps.

#### Step 4
After all previous step you should open settings.py file and change this strings:
``` Python
EMAIL_HOST_USER = "YOUR_ADDRESS@gmail.com"
EMAIL_HOST_PASSWORD = "PASSWORD"
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
DEFAULT_FROM_EMAIL = 'YOUR_ADDRESS@gmail.com'
```

#### Step 5 (optional)
If after previous steps mailing still doesn't work you should give access for host
use your google account

## Run server
Use this command for run server. <br/>
```Bash
python manage.py runserver
```

### Attention!
Don't use this project for production, it should use 
gunicorn or another server with static and another database

## Extensions used

- [Pillow](https://github.com/python-pillow/Pillow)
- [Bootstrap](https://github.com/twbs/bootstrap)
- [Crispy forms](https://github.com/django-crispy-forms/django-crispy-forms)
- [Allauth](https://github.com/pennersr/django-allauth)
- [Django extensions](https://github.com/django-extensions/django-extensions)
- [Ckeditor](https://github.com/django-ckeditor/django-ckeditor)
- [Google icons](https://fonts.google.com/icons)
- [otSlider](https://github.com/iniohd/otslider)
- [Splide](https://github.com/Splidejs/splide)
- [Django 3 jet](https://github.com/geex-arts/django-jet)
- [Django debug toolbar](https://github.com/jazzband/django-debug-toolbar)
- [Accordion](https://github.com/michu2k/Accordion)
