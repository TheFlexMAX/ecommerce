from django.contrib.auth import get_user_model
from django.db import models


User = get_user_model()


class Customer(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='customer',
        unique=True,
        verbose_name='User',
    )
    phone = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        verbose_name='Phone number',
    )
    orders = models.ManyToManyField(
        'order.Order',
        related_name='related_customer',
        null=True,
        blank=True,
        verbose_name='Customer orders',
    )

    class Meta:
        verbose_name = 'Buyer'
        verbose_name_plural = 'Buyers'

    def __str__(self):
        return f'Buyer {self.user.first_name} {self.user.last_name}'
