from django import forms

from allauth.account.forms import SignupForm

from user.models import Customer


class UserSignupForm(SignupForm):
    """
    Расширение формы регистрации для пользователя
    """
    phone = forms.CharField(
        required=False,
        max_length=20,
        widget=forms.TextInput(attrs={'placeholder': 'Номер телефона'})
    )
    first_name = forms.CharField(
        max_length=255,
        widget=forms.TextInput(attrs={'placeholder': 'Имя'})
    )
    last_name = forms.CharField(
        max_length=255,
        widget=forms.TextInput(attrs={'placeholder': 'Фамилия'})
    )

    # Put in custom signup logic
    def custom_signup(self, request, user):
        user.first_name = self.cleaned_data["first_name"]
        user.last_name = self.cleaned_data["last_name"]
        new_customer = Customer.objects.create(user=user)
        new_customer.phone = self.cleaned_data["phone"]
        new_customer.save()
        new_customer.user = user
        # Save the user's type to their database record
        user.save()
        return user
