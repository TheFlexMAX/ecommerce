from django.urls import path

from user.views import profile_view


urlpatterns = [
    # User
    path('', profile_view, name='profile'),
]
