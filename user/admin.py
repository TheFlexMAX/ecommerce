from django.contrib import admin

from user.models import Customer


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'phone')
    list_display_links = (
        'id',
        'user',
    )
    list_filter = ('user',)
    raw_id_fields = ('orders',)


admin.site.site_header = 'Buyers'
admin.site.site_title = 'Buyers'
