from django.shortcuts import render

from order.models import Order
from order.utils import get_active_customer_cart
from user.models import Customer


def profile_view(request):
    if request.user.is_authenticated:
        cart = get_active_customer_cart(request)
        customer = Customer.objects.get(user=request.user)
        orders = Order.objects.filter(customer=customer).order_by('-date_created')
        context = {
            'orders': orders,
            'cart': cart
        }
        return render(request, 'shop/profile.html', context)
    else:
        return render(request, 'account/login.html', {})
